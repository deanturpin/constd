#include "stod.h"
#include <benchmark/benchmark.h>

constexpr auto test_string1 = "0.0";
constexpr auto test_string2 = "12345.6789";

void BM_stod_constexpr(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(constd::stod(test_string1));
}

BENCHMARK(BM_stod_constexpr);

void BM_stod_constexpr2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(constd::stod(test_string2));
}

BENCHMARK(BM_stod_constexpr2);

void BM_stod(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(std::stod(test_string1));
}

BENCHMARK(BM_stod);

void BM_stod2(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(std::stod(test_string2));
}

BENCHMARK(BM_stod2);
