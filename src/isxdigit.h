#pragma once

#include <string>
#include <string_view>

// https://en.cppreference.com/w/cpp/string/byte/isxdigit

namespace constd {
using std::string_view_literals::operator""sv;

// Check for hexadecimals
constexpr bool isxdigit(int ch) {

  // All hexadecimals characters
  constexpr auto all_hex = "0123456789abcdefABCDEF"sv;

  auto c = static_cast<char>(ch);

  return all_hex.contains(c);
}

} // namespace constd

static_assert(constd::isxdigit('0'));
static_assert(constd::isxdigit('9'));
static_assert(constd::isxdigit('a'));
static_assert(constd::isxdigit('f'));
static_assert(constd::isxdigit('A'));
static_assert(constd::isxdigit('F'));
static_assert(not constd::isxdigit('g'));
static_assert(not constd::isxdigit('G'));
static_assert(not constd::isxdigit(' '));
static_assert(not constd::isxdigit('\n'));
static_assert(not constd::isxdigit('\b'));
