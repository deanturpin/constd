#include "isprint.h"
#include <benchmark/benchmark.h>
#include <cassert>
#include <cctype>
#include <cstdint>
#include <print>

constexpr auto not_printable_char = '\b';

void BM_isprint_constexpr(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(constd::isprint(false));
}

BENCHMARK(BM_isprint_constexpr);

void BM_isprint(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(std::isprint(not_printable_char));
}

BENCHMARK(BM_isprint);

// Test equivalence to the Standard Library
auto _ = []() {
  for (auto i = uint8_t{}; i < 0xf; i++) {
    assert(constd::isprint(i) == std::isprint(i));
  }
  return true;
}();

// Fuzz the constd function
void BM_isprint_fuzz_constexpr(benchmark::State &state) {
  char i = 0;
  for (auto _ : state)
    benchmark::DoNotOptimize(constd::isprint(i++));
}

BENCHMARK(BM_isprint_fuzz_constexpr);

// Fuzz the std function
void BM_isprint_fuzz(benchmark::State &state) {
  char i = 0;
  for (auto _ : state)
    benchmark::DoNotOptimize(std::isprint(i++));
}

BENCHMARK(BM_isprint_fuzz);

// constexpr fuzz
constexpr bool fuzz_isprint() {
  for (int i = 0; i < 0x1fff; ++i)
    constd::isprint(i);

  return true;
}
static_assert(fuzz_isprint());
