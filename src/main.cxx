#include "constd.h"

// Simple example to check we don't have a dependency on Google Benchmark
int main() { return constd::isprint(' ') ? 0 : 1; }