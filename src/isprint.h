#pragma once

#include <string>
#include <string_view>

// https://en.cppreference.com/w/cpp/string/byte/isprint

namespace constd {
constexpr bool isprint(int ch) {
  // Just check within printable ASCII range
  return ch > 31 && ch < 127;
}
} // namespace constd

// Pass character literals
static_assert(constd::isprint(' '));
static_assert(constd::isprint('a'));
static_assert(constd::isprint('A'));
static_assert(constd::isprint('!'));
static_assert(constd::isprint('~'));
static_assert(not constd::isprint('\n'));
static_assert(not constd::isprint('\b'));
static_assert(not constd::isprint('\t'));

// Pass integer literals
static_assert(not constd::isprint(31));
static_assert(constd::isprint(32));
static_assert(constd::isprint(126));
static_assert(not constd::isprint(127));
static_assert(not constd::isprint(255));
