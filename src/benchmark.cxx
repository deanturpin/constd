// #include "constd.h"
#include <benchmark/benchmark.h>

// Find and run the benchmarks
int main(int argc, char **argv) {
  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();
}