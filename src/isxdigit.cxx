#include "isxdigit.h"
#include <benchmark/benchmark.h>
#include <cctype>

constexpr auto char_false = 'X';

void BM_isxdigit_constexpr(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(constd::isxdigit(char_false));
}

BENCHMARK(BM_isxdigit_constexpr);

void BM_isxdigit(benchmark::State &state) {
  for (auto _ : state)
    benchmark::DoNotOptimize(std::isxdigit(char_false));
}

BENCHMARK(BM_isxdigit);
