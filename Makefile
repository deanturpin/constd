.SILENT:

all:
	cmake -B build -S .
	cmake --build build --parallel 
	build/constd_benchmark
	build/constd_main

entr:
	ls CMakeLists.txt Makefile src/* | entr -cr make --silent

stats:
	sloccount bin/ src/*.cxx src/CMakeLists.txt Makefile | grep -E "SLOC|Cost"

clean:
	$(RM) -r build
