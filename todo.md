# Things to do (people to see)

- [ ] `is_valid_mac` test
- [ ] `is_valid_ip` test
- [ ] Why are isprint implementations not equivalent for large integers?

```cpp
// Test equivalence to the Standard Library
auto _ = []() {
  for (auto i = uint8_t{}; i < 0xf; i++) {

    // for (auto i = uint8_t{}; i < UINT8_MAX; i++) {
    // if (constd::isprint(i) != std::isprint(i)) {
    //   std::print("std::isprint({}) == {}\n", i, std::isprint(i));
    //   std::print("isprint({}) == {}\n", i, constd::isprint(i));
    //   break;
    //   // assert(false);
    // }

    assert(constd::isprint(i) == std::isprint(i));
  }
  return true;
}();
```
