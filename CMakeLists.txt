cmake_minimum_required(VERSION 3.28.3)

project(constd
    VERSION 0.0.1
    LANGUAGES CXX
)

set(CMAKE_CXX_FLAGS "-std=c++23 -O1 -Wall -Wextra -Wpedantic -march=native -Wnrvo")
set(CMAKE_BUILD_TYPE Release)

if(APPLE)
    # Use the latest homebrew version
    set(CMAKE_CXX_COMPILER /usr/local/bin/clang++)
    set(CMAKE_C_COMPILER /usr/local/bin/clang)
elseif(LINUX)
    # Use the compiled version
    set(CMAKE_CXX_COMPILER /usr/local/bin/g++)
    set(CMAKE_C_COMPILER /usr/local/bin/gcc)
else()
    # godspeed
    message(WARNING "Platform unknown")
endif()

# LIBRARY - of constexpr functions
add_library(${PROJECT_NAME} STATIC src/constd.cxx)

# BENCHMARK - compare with Standard Library equivalents
find_package(benchmark REQUIRED)
add_executable(${PROJECT_NAME}_benchmark
    src/benchmark.cxx
    src/isprint.cxx
    src/isxdigit.cxx
    src/stod.cxx
)

target_link_libraries(${PROJECT_NAME}_benchmark
    constd
    benchmark::benchmark
)

# EXAMPLE - doesn't link against the benchmark library
add_executable(${PROJECT_NAME}_main src/main.cxx)
target_link_libraries(${PROJECT_NAME}_main constd)
