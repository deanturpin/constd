# `constexpr` all the things

[![](https://gitlab.com/deanturpin/constd/badges/main/pipeline.svg)](https://gitlab.com/deanturpin/constd/-/pipelines)

It's frustrating when you go all "in on" `constexpr`, only to be hamstrung by bits of the Standard Library that aren't yet `constexpr`.

See [the repo](https://gitlab.com/deanturpin/constd).

This is static library that provides an implementation for my [other projects](https://turpin.dev).

## Goals

- Use `static_assert` as compile-time unit testing
- Benchtest with Google Benchmark
- Compare performance with Standard Library implementation
- Make it straightforward to replace these temporary workarounds with the Standard Library implementations when they becomes `constexpr`

---

